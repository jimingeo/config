# Lines configured by zsh-newuser-install
HISTFILE=~/code/config/zsh/.histfile
HISTSIZE=30000
SAVEHIST=30000
setopt appendhistory autocd extendedglob nomatch notify
bindkey -v
# End of lines configured by zsh-newuser-install

# Custom configurations
# Source completion options
. ~/code/config/zsh/completion
# Fish like syntax highlighting
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Include fzf if available
if [ -f "/usr/share/doc/fzf/examples/key-bindings.zsh" ]; then
    . "/usr/share/doc/fzf/examples/key-bindings.zsh"
    #. "/usr/share/zsh/vendor-completions/_fzf"
fi

# Enable prompt theme option
autoload promptinit
promptinit
prompt walters

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/jgeorge/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/jgeorge/miniconda3/etc/profile.d/conda.sh" ]; then
        . "/home/jgeorge/miniconda3/etc/profile.d/conda.sh"
    else
        export PATH="/home/jgeorge/miniconda3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

if [ -e /home/jgeorge/.nix-profile/etc/profile.d/nix.sh ]; then . /home/jgeorge/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
