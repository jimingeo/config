**Nix based configuration**
This directory contains the nix configurations for setting up a consistent development environment.
The initial configuration is for neovim; the intention is to extend this configuration to things like
`zsh`, `screen`, `tmux`, etc., and eventually come up with a consistent base dev setup. 

**Configuration**
The first step, of course, is to install nix following instructions at:
https://nix.dev/tutorials/install-nix

After this, the configuration can be done at a user level by running `nix-env`. For example, if we want
to setup `neovim`, the following command can be run:
```shell
nix-env -f neovim.nix -i
```

Once the command downloads and installs all necessary "packages", `neovim` will be available for
use.
