{pkgs ? import <nixpkgs> {}}:

{
  myNeoVim = pkgs.neovim.override {
    configure = {
      customRC = ''
set smarttab
set autoindent
set background=dark
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set textwidth=100
set shiftround
set number
set relativenumber
set showmode
set showcmd
set incsearch
set hlsearch
set splitbelow
set splitright
set foldlevel=2
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()

"Remap arrow keys to avoid using them
"In normal mode, map arrow keys to navigate split windows
nnoremap <Down> <c-w>j
nnoremap <Up> <c-w>k
nnoremap <Left> <c-w>h
nnoremap <Right> <c-w>l
"..and map tab key to navigate tabs
nnoremap <tab> :tabnext<CR>
"nnoremap <S-Tab> :tabprevious<CR>

let mapleader = "\<Space>"
nnoremap <Leader>w :w<CR>

set hidden
set path=.,**
nnoremap <Leader>f :find<Space>
nnoremap <Leader>o :FZF<Enter>
nnoremap <F9> :b#<return>
nnoremap <Leader>b :ls<CR>:b<Space>
nnoremap <PageDown> :bnext<CR>
nnoremap <PageUp> :bprevious<CR>

nnoremap <Leader>t :Texplore<CR>
nnoremap <Leader>v :Vexplore<CR>
nnoremap <Leader>s :Sexplore<CR>

nnoremap <Leader>x :close<CR>
      '';
      packages.myVimPackage = with pkgs.vimPlugins; {
        start = [
          onedark-nvim
          fzf-vim
          vim-nix
          vim-yaml
          (nvim-treesitter.withPlugins (p: [ p.bash p.comment p.cpp p.dockerfile p.go p.haskell p.json
                                             p.lua p.make p.nix p.python p.rust p.sql ]))
        ];
      };
    };
  };
}
