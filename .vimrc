"General settings -----------{{{
syntax on
filetype on
filetype plugin on
filetype indent on
set smarttab
set autoindent
set background=dark
set tabstop=4
set expandtab
set softtabstop=4
set shiftwidth=4
set number
set textwidth=100
set shiftround
set relativenumber
"set tags=./tags;
set showmode
set showcmd
set incsearch
set hlsearch
set splitbelow
set splitright
" }}}

"set ruler

"Set status line to show file name, current line and total lines
"Status line group ------------{{{
set statusline=%f   "File name
set statusline+=%=  "Move to right side
set statusline+=%l/%L
set statusline+=:%c
set laststatus=2
" }}}

highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/

"Remap arrow keys to avoid using them
"In normal mode, map arrow keys to navigate split windows
nnoremap <Down> <c-w>j
nnoremap <Up> <c-w>k
nnoremap <Left> <c-w>h
nnoremap <Right> <c-w>l
"..and map tab key to navigate tabs
nnoremap <tab> :tabnext<CR>
"nnoremap <S-Tab> :tabprevious<CR>

let mapleader = "\<Space>"
nnoremap <Leader>w :w<CR>

" Buffer management -----------{{{
set hidden
set path=.,**
nnoremap <Leader>f :find<Space>
nnoremap <Leader>o :FZF<Enter>
nnoremap <F9> :b#<return>
nnoremap <Leader>b :ls<CR>:b<Space>
nnoremap <PageDown> :bnext<CR>
nnoremap <PageUp> :bprevious<CR>
" Couple of additional mappings for buffer navigation with mac keyboard:
" to make 'fn+<Down>'==>'<PageDown>' and 'fn+<Up>'==>'<PageUp>'
nnoremap [1;2A :bnext<CR>
nnoremap [1;2B :bprevious<CR>
nnoremap <S-Up> :bprevious<CR>
nnoremap <S-Down> :bnext<CR>
" }}}

nnoremap <Leader>t :Texplore<CR>
nnoremap <Leader>v :Vexplore<CR>
nnoremap <Leader>s :Sexplore<CR>

nnoremap <Leader>x :close<CR>

"Autocommands
"Python autocommand group ------------{{{
augroup filetype_python
    autocmd FileType python nnoremap <localleader>c I#<esc>
    autocmd FileType python setlocal foldmethod=indent
    autocmd FileType python setlocal foldlevel=1
    autocmd FileType python setlocal complete=.,w,b,u,t
augroup END
" }}}

"VIM autocommand group ------------{{{
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
    autocmd FileType vim nnoremap <localleader>c I"<esc>
augroup END
" }}}
